import { assert, expect, should } from "chai";
import { describe } from "mocha";
import calc from "../src/calc.js";

describe("calc.js", () =>  {
    let myVar;
    before(() => {
        myVar = 0;
    });
    beforeEach(() => myVar++);
    after(() => console.log(`myVar: ${myVar}`));
    it("can add numbers", () => {
        expect(calc.add(2, 2)).to.equal(4);
        expect(calc.add(5, 2)).to.equal(7);
    });
    it("can subtract numbers", () => {
        //expect(calc.subtract(5, 1)).to.equal(4);
        assert((calc.subtract(5, 1)) == (4));
        assert((calc.subtract(6, 1)) == (5));
    });
    it("can multiply numbers", () => {
        expect(calc.multiply(3, 3)).to.equal(9)
        should().exist(calc.multiply);
    });
    it("0 division throws an error", () => {
        const err_msg = "0 division not allowed";
        expect(() => calc.divide(1, 0)).to.Throw(err_msg);
    });
    it("can divide numbers", () => {
        expect(calc.divide(6, 2)).to.equal(3)
    });
});
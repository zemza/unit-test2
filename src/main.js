import express from "express";
import calc from "./calc.js";
/*
// src/main.js
const express = require('express');
const mylib = require('./src/calc.js');
const app = express();
const port = 3000;
const host = "localhost";
// Endpoint - GET http://localhost:3000/
app.get('/', (req, res) => {
res.send('Hello world');
});
app.get('/add', (req, res) => {
const a = parseInt(req.query.a);
const b = parseInt(req.query.b);
res.send(mylib.sum(a,b));
});
app.listen(port, host, () => {
console.log(`Server: http://localhost:${port}`);
});
*/
const app = express();
const port = 3000;
const host = "localhost";
//Endpoint - GET http://localhost:3000/
app.get('/', (req, res) => {
    res.status(200).send("Hello world!");
});

//Endpoint - GET http://localhost:3000/add?=a=2&b=3
app.get('/add', (req, res) => {
    const a = req.query.a;
    const b = req.query.b;
    const sum = calc.add(a, b);
    res.status(200).send(sum.toString());
});

app.listen(port, host, () => {
    console.log(`http://${host}:${port}`);
});
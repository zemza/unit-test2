// src/calc.js
/**
* Adds two numbers together
* @param {number} a
* @param {number} b
* @returns {number}
*/
const add = (a, b) => a + b;
/**
* Subtracts number from minuend
* @param {number} minuend
* @param {number} subtrahend
* @returns {number} difference
*/
const subtract = (minuend, subtrahend) => {
return minuend - subtrahend;
}
/**
 * Divide two numbers
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error}
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed");
    const fraction = dividend / divisor;
    return fraction;
}
/**
 * Multiplies two numbers together
 * @param {number} multiA
 * @param {number} multiB
 * @returns {number}
 */
const multiply = (multiA, multiB) => multiA * multiB;

export default { add, subtract, divide, multiply }